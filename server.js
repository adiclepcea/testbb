
var express = require('express');
var bodyParser = require('body-parser')
var myApp = express();

myApp.use(express.static(__dirname+"/"));
myApp.use(bodyParser.json());

var id = 1;
var port = 3000;

var blog = {
  id:id,
  author: "Adi",
  title: "Adi\'s blog",
  url: "https://adisblog.ro"
};
id = id+1;
var blogs = [blog];

myApp.get("/api/blogs", function(req, res){
  res.send(blogs);
});

myApp.post("/api/blogs",function(req, res){
  console.log("saving new");
  blogs = blogs.concat([req.body]);
  id = id+1;

  res.send(req.body);
  res.end();
});

myApp.put("/api/blogs/:id",function(req, res){
  console.log("saving modified:"+req.params.id);
  blog = req.body;
  for(var i=blogs.length - 1; i>=0; i--){
    if(blogs[i].id==req.params.id){
        blogs[i].author = blog.author;
        blogs[i].title = blog.title;
        blogs[i].url = blog.url;
        break;
      }
  }
  res.send(req.body);
  res.end();
});

myApp.delete("/api/blogs/:id",function(req, res){
  console.log("deleting existing "+req.params.id);

  for(var i=blogs.length - 1; i>=0; i--){
    if(blogs[i].id==req.params.id){
        blogs.splice(i,1);
      }
  }
  res.send({id: req.params.id});
});

myApp.listen(port);

console.log("server on "+port);
