Backbone.Model.prototype.idAttribute = "id";
var app = app || {};

app.Blog = Backbone.Model.extend({
  defaults: {
    author: "",
    title: "",
    url: "http://localhost:3000/api/blogs"
  }
});

app.Blogs = Backbone.Collection.extend({
  url:"http://localhost:3000/api/blogs"
});

blog1 = new app.Blog({
  author: "Adi",
  title: "Adi's Blog",
  url: "http://adisblog.com"
});

blog2 = new app.Blog({
  author: "Roli",
  title: "Roli's Blog",
  url: "http://rolisblog.com"
});

//blogs = new app.Blogs([blog1, blog2]);
blogs = new app.Blogs();

app.BlogView = Backbone.View.extend({
  model: new app.Blog(),
  tagName: "tr",
  initialize: function(){
    this.template = _.template($("#blogs-list-template").html());
  },
  events:{
    'click .edit-blog':"edit",
    'click .cancel-blog':"cancel",
    'click .update-blog':"update",
    'click .delete-blog':"delete"

  },
  edit: function(){
    $(".edit-blog").hide();
    $(".delete-blog").hide();
    this.$(".update-blog").show();
    this.$(".cancel-blog").show();
    var author = this.$(".author").html();
    var title = this.$(".title").html();
    var url = this.$(".url").html();
    this.$(".author").html('<input type="text" class="form-control" id="author-update" value="'+author+'"/>');
    this.$(".title").html('<input type="text" class="form-control" id="title-update" value="'+title+'"/>');
    this.$(".url").html("<input type='text' class='form-control' id='url-update' value='"+url+"'/>");
  },
  cancel: function(){
    $(".edit-blog").show();
    $(".delete-blog").show();
    this.render();
  },
  delete: function(){
    if(confirm("Are you sure you want to delete "+this.model.get("title")+"?")){
      this.model.destroy({
        success: function(response){
          console.log("deleted "+response.responseText);
        },
        error: function(){
          console.log("failed delete");
        }
      });

    }
  },
  update: function(){
    blogs.remove(this.model);
    blogs.add(new app.Blog({
      "author":this.$("#author-update").val(),
      "title":this.$("#title-update").val(),
      "url":this.$("#url-update").val()
    }));
    this.$(".author").val("");
    this.$(".title-input").val("");
    this.$(".url-input").val("");
    this.model.save(null,{
        success: function(response){
          console.log("OK");
        },
        error:function(response){
          console.log("NOK");
        }

    });
  },
  render: function(){
    console.log(this.model.toJSON());
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  }
});

app.BlogsView = Backbone.View.extend({
  model: blogs,
  el: $("#blogs-list"),
  initialize: function(){
    this.model.on('add remove', this.render,this);
    if(this.model.length>0){
      this.render();
    }
    this.model.fetch({
      success: function(response){
        _.each(response.toJSON(), function(item){
          console.log(item);
        })
      },
      error: function(){
        console.log("failed");
      }
    });
    return this;
  },
  render: function(){
    var self=this;
    this.$el.html("");
    _.each(this.model.toArray(), function(blog){
      self.$el.append((new app.BlogView({model:blog})).render().$el);
    });
    return this;
  }
});

var blogsView = new app.BlogsView();

$().ready(function(){
  $(".add-blog").on('click',function(){
    var blog = new app.Blog({
      author: $(".author-input").val(),
      title: $(".title-input").val(),
      url: $(".url-input").val()
    });
    $(".author-input").val("");
    $(".title-input").val("");
    $(".url-input").val("");
    //console.log(blog.toJSON());
    blogs.add(blog);
    blog.save(null,{
      success: function(response){
        console.log("Saved");
      },
      error: function(err){
        console.log("failed to save:"+err.toJSON());
      }
    });
  })
});
